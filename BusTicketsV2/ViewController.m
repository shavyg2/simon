//
//  ViewController.m
//  BusTickets
//
//  Created by Simon de Almeida on 2013-09-22.
//  Copyright (c) 2013 Simon de Almeida. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
    [self rebuildThePicker];
    _lcTotalTickets.text = [ticketsArray objectAtIndex:0];
}

-(void)viewWillAppear:(BOOL)animated{
    [self viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [[ticketsArray objectAtIndex:row] description];
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // report the selection to the UI label
    _lcTotalTickets.text = [NSString stringWithFormat:@"%@", [[ticketsArray objectAtIndex:row] description]];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_model availableSeatsForRoute:(_scLocations.selectedSegmentIndex)] + 1;
}


-(NSString *)getCurrTime{
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    
    dateString = [formatter stringFromDate:[NSDate date]];
    return dateString;
}
- (IBAction)btnBuyTickets:(id)sender {
    
    NSInteger row =[_pvTickets selectedRowInComponent:0];
    if(row){
        _lcTicketsBought.text = [NSString stringWithFormat:@"Bought: %@", [[ticketsArray objectAtIndex:row] description]];
        
        [_model buySeats:row forRoute: _scLocations.selectedSegmentIndex];
        [self rebuildThePicker];
        
        _model.history = [NSString stringWithFormat:@"%@\n%@ - %@\n\n%@",[self getCurrTime],[_scLocations titleForSegmentAtIndex:[_scLocations selectedSegmentIndex]],_lcTicketsBought.text,_model.history];
        
        //[self.model.plistData setObject:[_model history]forKey:@"purchaseHistory"];
        [_model.plistData writeToFile:[[_model plistPath] path] atomically:YES];
        
        //[self.model.plistData setObject:[NSString stringWithFormat:@"%d",[_model availableSeatsForRoute:_scLocations.selectedSegmentIndex]] forKey:@"purchaseHistory"];
        
    }else
        _lcTicketsBought.text = @"You cannot buy 0 tickets!";
}

- (IBAction)scLocationsSelected:(id)sender {
    [self rebuildThePicker];
    _lcTotalTickets.text = [ticketsArray objectAtIndex:0];
}

- (void)rebuildThePicker{
    
    [ticketsArray removeAllObjects];
    ticketsArray = [[NSMutableArray alloc] init];
    
    int avalSeats = [_model availableSeatsForRoute:_scLocations.selectedSegmentIndex];
    float ticketPrice = [_model ticketCostForRoute:_scLocations.selectedSegmentIndex];
    
    for (int seats = 0; seats <= avalSeats; seats++)
        [ticketsArray addObject: [NSString stringWithFormat:@"%d x %1.2f = %1.2f",seats,ticketPrice,(seats* ticketPrice)]];
    
    [_pvTickets reloadAllComponents];
    [_pvTickets selectRow:0 inComponent:0 animated:YES];
}
@end
