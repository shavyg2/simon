//
//  History.m
//  BusTicketsV2
//
//  Created by Simon de Almeida on 2013-09-30.
//  Copyright (c) 2013 Simon de Almeida. All rights reserved.
//

#import "History.h"

@interface History ()

@end

@implementation History

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated{
    _tvHistory.text = [_model history];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
