//
//  main.m
//  BusTicketsV2
//
//  Created by Simon de Almeida on 2013-09-30.
//  Copyright (c) 2013 Simon de Almeida. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
