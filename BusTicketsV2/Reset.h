//
//  Reset.h
//  BusTicketsV2
//
//  Created by Simon de Almeida on 2013-09-30.
//  Copyright (c) 2013 Simon de Almeida. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Reset : UIViewController

- (IBAction)resetApp:(id)sender;
@property (nonatomic, strong) Model *model;
@end
