//
//  AppDelegate.m
//  BusTicketsV2
//
//  Created by Simon de Almeida on 2013-09-30.
//  Copyright (c) 2013 Simon de Almeida. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "History.h"
#import "Reset.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Recommended (required) pattern for a tab bar app
    
    // Get a reference to the tab bar controller
    UITabBarController *tbc = (UITabBarController *)self.window.rootViewController;
    
    // Get a reference to specific view controllers
    ViewController *vc1 = (ViewController *)[tbc.viewControllers objectAtIndex:0];
    History *vc2 = (History *)[tbc.viewControllers objectAtIndex:1];
    Reset *vc3 = (Reset *)[tbc.viewControllers objectAtIndex:2];
    
    //SecondViewController *vc2 = (SecondViewController *)[tbc.viewControllers objectAtIndex:1];
    
    // Create and assign a model object, to all controllers that need it
    Model *model = [[Model alloc] init];
    vc1.model = model;
    vc2.model = model;
    vc3.model = model;
    
    // Override point for customization after application launch.
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
