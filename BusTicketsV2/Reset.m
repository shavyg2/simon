//
//  Reset.m
//  BusTicketsV2
//
//  Created by Simon de Almeida on 2013-09-30.
//  Copyright (c) 2013 Simon de Almeida. All rights reserved.
//

#import "Reset.h"

@interface Reset ()

@end

@implementation Reset

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)resetApp:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Reset values?"
                                message:@"You have requested to reset the application to its initial state."
                               delegate:self
                      cancelButtonTitle:@"Cancel"
                      otherButtonTitles:@"Reset", nil];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
		case 1:
			// Reset
            [_model resetApp];
			break;
	}
}
@end
