//
//  ViewController.h
//  BusTickets
//
//  Created by Simon de Almeida on 2013-09-22.
//  Copyright (c) 2013 Simon de Almeida. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>
{
    NSMutableArray *ticketsArray;
}

@property (nonatomic, strong) Model *model;

@property (weak, nonatomic) IBOutlet UISegmentedControl *scLocations;
@property (weak, nonatomic) IBOutlet UIPickerView *pvTickets;
@property (weak, nonatomic) IBOutlet UILabel *lcTotalTickets;
@property (weak, nonatomic) IBOutlet UILabel *lcTicketsBought;


- (IBAction)btnBuyTickets:(id)sender;
- (IBAction)scLocationsSelected:(id)sender;
- (void)rebuildThePicker;

-(NSString *)getCurrTime;
@end
